<!DOCTYPE html>
<html>
<head>
	<title>Image Upload</title>
	<link rel="stylesheet" href="../../public/css/bootstrap.min.css" crossorigin="anonymous">
	<link rel="stylesheet" href="../../public/css/style.css" crossorigin="anonymous">
</head>
<body>

	<div class="wraper">
		<div class="container">
			<div class="menu">
				<ul>
					<li>
						<a href="../../index.php">Home</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="container">
			<div class="card">
				<div class="card-header">
					<h2 class="text-center">Image Upload</h2>
				</div>
				<div class="card-body">
					<form action="create.php" method="POST" enctype="multipart/form-data"  class="form-group">
						<label> Client Name </label>
						<input type="text" name="c_name" value="" required class="form-control">

						<label> Image Name </label>
						<input type="file" name="image_name" value="" required class="form-control">
						<br>
						<button class="btn btn-outline-primary btn-sm fiftyparsent" type="submit">Upload</button>
					</form>
				</div>
				<div class="card-footer">
					<p>From Footer</p>
				</div>
			</div>
		</div>
	</div>
	
<!--Bootstrap java scritpt code -->
<script src="../../public/js/jquery-3.2.1.slim.min.js" crossorigin="anonymous"></script>
<script src="../../public/js/popper.min.js"  crossorigin="anonymous"></script>
<script src="../../public/js/bootstrap.min.js" crossorigin="anonymous"></script>

</body>
</html>